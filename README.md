# videoTest


### Development

```bash
$ npm i
$ npm run dev
$ open http://localhost:7001/
```

### Deploy

```bash
$ npm start
$ npm stop
```

### 注意
需要在uploadjs补充oss的配置<br />
删除文件可以每个上传生成一个文件夹，删除文件夹，或者是有fs-extra的emptyDir，目前这里demo测试，当真正应用到线上时肯定不能直接删除全部文件<br />
oss我不熟，不知道怎么多文件上传，如何可以就方便点
