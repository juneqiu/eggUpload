const Controller = require('egg').Controller;
const fs = require('mz/fs');
let OSS = require('ali-oss');

let aliossInfo = {
    region: '',
    bucket: '',
    accessKeyId: '',
    accessKeySecret: ''
}

let client = new OSS(aliossInfo);

// 很多问题没有处理，只是实现了一个demo
class UpLoad extends Controller {
    async upload() {
        const { ctx } = this;
        const file = ctx.request.files[0];
        const frameCount = 10
        const res = await this.service.ffmpeg.videoPoster(file.filepath, 1, frameCount)
        let posterResult
        try {
            posterResult = await client.put(res.name, res.files[frameCount - 1]);
        } finally {
            res.files.forEach(item => {
                fs.unlink(item);
            })
        }
        let result;
        try {
            result = await client.put(file.filename, file.filepath);
        } finally {
            await fs.unlink(file.filepath);
        }
        ctx.body = {
            code: 200,
            msg: '2020/6/25',
            data: {
                poster: posterResult.url,
                video: result.url
            }
        }
    }

    async ExtractSound() {
        const { ctx } = this;
        const file = ctx.request.files[0];
        const res = await this.service.ffmpeg.ExtractVideoSound(file.filepath)
        ctx.body = {
            code: 200,
            msg: '2020/6/26',
            data: res
        }
    }

    async addLogo() {
        const { ctx } = this;
        const file = ctx.request.files[0];
        const res = await this.service.ffmpeg.addVideoLogo(file.filepath)
        ctx.body = {
            code: 200,
            msg: '2020/6/26',
            data: res
        }
    }
}

module.exports = UpLoad;
