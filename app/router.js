'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
    const { router, controller } = app;
    router.get('/', controller.home.index);
    router.post('/alioss/upload', controller.upload.upload);
    router.post('/alioss/extract', controller.upload.ExtractSound);
    router.post('/alioss/addlogo', controller.upload.addLogo);
};
