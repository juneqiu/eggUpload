'use strict';
const Service = require('egg').Service;
const ffmpeg = require('ffmpeg')
const path = require('path');

class Ffmpeg extends Service {


    randomString(len) {
        len = len || 32;
        var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
        var maxPos = $chars.length;
        var pwd = '';
        for (let i = 0; i < len; i++) {
            pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
        }
        return pwd;
    }

    /**
     * videoPoster
     * @desc 截取视频某帧作为封面
     * @param { String } videoPath // 路径
     * @param { Number } frameRate 每秒的帧数
     * @param { Number } frameCount // 截取的帧数
     */
    videoPoster(videoPath, frameRate, frameCount) {
        return new Promise((resolve, reject) => {
            new ffmpeg(videoPath, (err, video) => {
                if (!err) {
                    const target = path.join(this.config.baseDir, `app/public/videoPoster`)
                    const name = this.randomString()
                    video.fnExtractFrameToJPG(target, {
                        frame_rate: frameRate,
                        number: frameCount,
                        file_name: name
                    }, (error, files) => {
                        if (!error) {
                            resolve({
                                files,
                                name
                            })
                        } else {
                            reject(error)
                        }

                    })
                } else {
                    reject(err)
                }
            })
        })
    }

    /**
     * ExtractVideoSound
     * @desc 提取视频的音频保存为mp3
     * @param { String } videoPath // 路径
     */
    ExtractVideoSound(videoPath) {
        return new Promise((resolve, reject) => {
            new ffmpeg(videoPath, (err, video) => {
                if (!err) {
                    const target = path.join(this.config.baseDir, `app/public/videoPoster`)
                    const name = this.randomString()
                    video.fnExtractSoundToMP3(`${target}/${name}.mp3`, (error, file) => {
                        if (!error) {
                            resolve(file)
                        } else {
                            reject(error)
                        }
                    })
                } else {
                    reject(err)
                }
            })
        })
    }

    /**
     * addVideoLogo
     * @desc 为视频添加水印
     * @param { String } videoPath // 路径
     */
    addVideoLogo(videoPath) {
        return new Promise((resolve, reject) => {
            new ffmpeg(videoPath, (err, video) => {
                if (!err) {
                    const target = path.join(this.config.baseDir, `app/public/videoPoster`)
                    const name = this.randomString()
                    /**
                     * @watermarkPath 存储图像的完整路径
                     * @newFilepath 新视频的名称。如果未指定，将由该函数创建
                     * @setting {
                        position		: "SW"		// Position: NE NC NW SE SC SW C CE CW
                        , margin_nord		: null		// Margin nord
                        , margin_sud		: null		// Margin sud
                        , margin_east		: null		// Margin east
                        , margin_west		: null		// Margin west
                        };
                    */
                    const watermarkPath = `${this.config.baseDir}/app/public/logo/logo.jpeg`
                    const newFilepath = `${target}/${name}.mp4`
                    const setting = {
                        position: 'SW'
                    }
                    video.fnAddWatermark(watermarkPath, newFilepath, setting, (error, file) => {
                        if (!error) {
                            resolve(file)
                        } else {
                            reject(error)
                        }
                    })
                } else {
                    reject(err)
                }
            })
        })
    }


}

module.exports = Ffmpeg;